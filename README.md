# ansible-limesurvey

Ansible role to set up prerequisites (nginx, php, postgresql databases, etc.) for [LimeSurvey](https://limesurvey.com).

